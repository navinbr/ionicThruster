# Investigating Best Practices for Maximizing Ion Propulsion Efficiency

This repository contains the code and documentation for a research project investigating best practices for maximizing the efficiency of ion propulsion. The project is being conducted by Navinchandry Bittencourt Ruas as part of their undergraduate engineering studies at UniCEUB.

## Introduction

Ion propulsion is an advanced space propulsion technology that uses electric fields to accelerate charged particles and generate thrust. Compared to traditional chemical rockets, ion propulsion offers much higher specific impulse and fuel efficiency, making it an attractive option for long-duration space missions.

The goal of this research project is to investigate the best practices for maximizing the efficiency of ion propulsion, with a focus on next-generation ion thrusters. The project will involve a combination of theoretical analysis, numerical simulations, and experimental testing to identify the most efficient ion propulsion configurations and operating conditions.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

The author would like to thank their academic advisor for their guidance and support throughout this project.
